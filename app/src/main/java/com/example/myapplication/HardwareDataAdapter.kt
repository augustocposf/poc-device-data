package com.example.myapplication

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.model.Data


class HardwareDataAdapter(private val itemList: MutableList<Data>) : RecyclerView.Adapter<HardwareDataViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HardwareDataViewHolder {
        return HardwareDataViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.hardware_data_item, parent, false))
    }

    override fun getItemCount(): Int = itemList.size


    override fun onBindViewHolder(holder: HardwareDataViewHolder, position: Int) =
            holder.bind(itemList[position])


    fun addHardwareData(hardwareData: Data) {
        itemList.add(hardwareData)
        notifyDataSetChanged()
    }
}