package com.example.myapplication

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.model.Data

class HardwareDataViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {


    fun bind(item: Data) {
        val dataItemText: TextView = itemView.findViewById(R.id.hardware_data_text)
        dataItemText.text = "${item.label}: ${item.deviceData}"
    }
}