package com.example.myapplication.util

object ImeiUtil {
    fun validateImei(imei: String): Boolean{
        return try {
            var sum = 0
            for (i in 0..14) {
                sum = (sum
                        + calculateDigit(imei[i].toString().toInt(), (i + 1) % 2 == 0))
            }
            if (sum % 10 == 0) true else false
        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
    }
    private fun calculateDigit(noDigit: Int, indexKey: Boolean): Int {
        return if (indexKey) {
            var number = noDigit * 2
            while (number > 9) {
                number = number.toString()[0].toString().toInt() + number.toString()[1].toString()
                    .toInt()
            }
            number
        } else {
            noDigit
        }
    }
}