package com.example.myapplication.DataHolders

import android.os.Build

object OSDataHolder {

    fun getOSVersion(): String = "${Build.VERSION.SDK_INT}"

    fun getOSVersionName(): String {
        val fields = Build.VERSION_CODES::class.java.fields
        var codeName = "UNKNOWN"
        fields.filter { it.getInt(Build.VERSION_CODES::class) == Build.VERSION.SDK_INT }
            .forEach { codeName = it.name }
        return Build.VERSION.RELEASE
    }
}