package com.example.myapplication.DataHolders

import android.app.ActivityManager
import android.content.Context
import android.os.Build
import android.os.Environment
import android.os.StatFs
import android.util.Log
import java.io.*
import java.text.DecimalFormat
import java.util.regex.Matcher
import java.util.regex.Pattern


object StorageDataHolder {
    private const val KILOBYTE: Long = 1024
    private const val KILOBIT: Int = 1000
    private fun externalMemoryAvailable(): Boolean {
        return Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED
    }

    fun getInternalMemorySize(): Long {
        val internalMemory: File = Environment.getDataDirectory()
        val androidOS: File = Environment.getRootDirectory()
        val stat = StatFs(internalMemory.path)
        val statOS = StatFs(androidOS.path)
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2) {
            val blockSize = stat.blockSizeLong
            val totalBlocks = stat.blockCountLong
            val availableBlocks = stat.availableBlocksLong
            val totalSpace = stat.totalBytes
            Log.d(
                "memoryTeste",
                "getInternalMemorySize: $totalSpace ${statOS.totalBytes / (KILOBYTE * KILOBYTE * KILOBYTE)}"
            )
            return totalSpace
        }
        return 0
    }

    fun getInternalMemorySizeGB(): String {
        return "${roundToNearest(bytesToGiga(getInternalMemorySize()))} GB"
    }


    fun getData() {
        val internalStatFs = StatFs(Environment.getStorageDirectory().absolutePath)
        val internalTotal: Long
        val internalFree: Long

        val externalStatFs = StatFs(Environment.getDataDirectory().absolutePath)
        val externalTotal: Long
        val externalFree: Long

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            internalTotal =
                internalStatFs.blockCountLong * internalStatFs.blockSizeLong / (KILOBYTE * KILOBYTE * KILOBYTE)
            internalFree =
                internalStatFs.availableBlocksLong * internalStatFs.blockSizeLong / (KILOBYTE * KILOBYTE * KILOBYTE)
            externalTotal = externalStatFs.blockCountLong * externalStatFs.blockSizeLong
            externalFree =
                externalStatFs.availableBlocksLong * externalStatFs.blockSizeLong / (KILOBYTE * KILOBYTE * KILOBYTE)
        } else {
            internalTotal =
                internalStatFs.blockCount.toLong() * internalStatFs.blockSize.toLong() / (KILOBYTE * KILOBYTE)
            internalFree =
                internalStatFs.availableBlocks.toLong() * internalStatFs.blockSize.toLong() / (KILOBYTE * KILOBYTE)
            externalTotal =
                externalStatFs.blockCount.toLong() * externalStatFs.blockSize.toLong() / (KILOBYTE * KILOBYTE)
            externalFree =
                externalStatFs.availableBlocks.toLong() * externalStatFs.blockSize.toLong() / (KILOBYTE * KILOBYTE)
        }
        Log.d("memoryTeste", "getData: $externalTotal $externalFree")
        val total = internalTotal + externalTotal
        val free = internalFree + externalFree
        val used = total - free
        val size = File("/data").totalSpace / (1024.0 * 1024 * 1024);


        val sizesystem = File("/system").totalSpace / (1024.0 * 1024 * 1024);

        val sizedev = File("/dev").totalSpace / (1024.0 * 1024 * 1024);

        val sizecache = File("/cache").totalSpace / (1024.0 * 1024 * 1024);

        val sizemnt = File("/mnt").totalSpace / (1024.0 * 1024 * 1024);

        val sizevendor = File("/vendor").totalSpace / (1024.0 * 1024 * 1024);

        val sizeEmulated = File("/").totalSpace / (1024.0 * 1024 * 1024);

//        val total = (sizemnt + sizedev + sizesystem + size + sizevendor + sizecache + sizeEmulated);
//        Log.e("total111", "$total $sizeEmulated");
    }

    fun getRamAmount(): Long {
        var reader: RandomAccessFile? = null
        var load: String? = null
        val twoDecimalForm = DecimalFormat("#.##")

        try {
            reader = RandomAccessFile("/proc/meminfo", "r")
            load = reader.readLine()

            val p: Pattern = Pattern.compile("(\\d+)")
            val m: Matcher = p.matcher(load)
            var value = ""
            while (m.find()) {
                value = m.group(1)
                // System.out.println("Ram : " + value);
            }
            reader.close()

//            Log.d("Teste", "getRamAmount: ${value.toLong()}")
            return (value.toLong() * 1024)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return 0
    }

    fun getRamAmountGB(): String {
        Log.d("Teste", "getRamAmountGB: ${getRamAmount()} ${(KILOBIT * KILOBIT)}")
        return "${"%.0f".format(getRamAmount() / ((KILOBIT * KILOBIT * KILOBIT).toDouble()))} GB"
    }

    private fun getSingleSdCard(): File? {
        val mountedCards = getExternalSd()

        var sdCardPath: String = ""
        if (mountedCards.isNotEmpty()) {
            sdCardPath = mountedCards[0]
        }
        if (sdCardPath.isNotBlank()) {
            if (sdCardPath.contains(":")) {
                sdCardPath = sdCardPath.substring(0, sdCardPath.indexOf(":"))
            }
            val externalSd = File(sdCardPath)

            if (externalSd.exists()) {
                return externalSd
            }
        }
        return null
    }

    fun getRamUpdated(context: Context): String{
        val actManager: ActivityManager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val memInfo = ActivityManager.MemoryInfo()
        actManager.getMemoryInfo(memInfo)
        val totalMemory = memInfo.totalMem / (KILOBYTE * KILOBYTE)
        return "$totalMemory MB"
    }

    fun getSdCardSize(): Long {
        val externalSd = getSingleSdCard()
        if (externalSd != null) {
            return externalSd.totalSpace
        }
        return 0
    }

    fun getSdCardSizeGB(): String {
        return "${"%.0f".format(getSdCardSize() / (KILOBIT * KILOBIT * KILOBIT).toDouble() )} GB"
    }

    fun hasSdCard(): String {
        if (getSingleSdCard() != null) {
            return "Sim"
        } else {
            return "Não"
        }

    }

    private fun getExternalSd(): ArrayList<String> {
        val sdDirList = ArrayList<String>()
        try {
            val dis = DataInputStream(FileInputStream("/proc/mounts"))
            val br = BufferedReader(InputStreamReader(dis))
            val externalDir = Environment.getExternalStorageDirectory().path
            while (true) {
                val strLine = br.readLine()
                if (strLine == null) {
                    break
                } else if (!(strLine.contains("asec")
                            || strLine.contains("legacy")
                            || strLine.contains("Android/obb"))
                ) {
                    if (strLine.startsWith("/dev/block/vold/")
                        || strLine.startsWith("/dev/block/sd")
                        || strLine.startsWith("/dev/fuse")
                        || strLine.startsWith("/mnt/media_rw")
                    ) {
                        val lineElements =
                            strLine.split(" ".toRegex()).dropLastWhile { it.isEmpty() }
                                .toTypedArray()
                        val path = File(lineElements[1])
                        if ((path.exists()
                                    || path.isDirectory
                                    || path.canWrite())
                            && path.exists()
                            && !path.path.contains("/system")
                            && !sdDirList.contains(lineElements[1])
                            && lineElements[1] != externalDir
                            && lineElements[1] != "/storage/emulated"
                            && !sdDirList.any {
                                it.endsWith(
                                    lineElements[1]
                                        .substring(
                                            lineElements[1].lastIndexOf("/"),
                                            lineElements[1].length
                                        )
                                )
                            }
                        ) {
                            sdDirList.add(lineElements[1])
                        }
                    }
                }
            }
            dis.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return sdDirList
    }

    private fun bytesToGiga(bytes: Long): Int = (bytes / (KILOBYTE * KILOBYTE * KILOBYTE)).toInt()
    private fun kiloToGiga(kilobytes: Long): Long = kilobytes / (KILOBYTE * KILOBYTE)

    fun roundToNearest(num: Int): Int {
        var count: Double = 0.0
        var result: Double = 0.0
        while (num > result) {
            result = Math.pow(2.0, ++count)
        }
        return result.toInt()
    }
    fun Double.round(decimals: Int = 2): Double = "%.${decimals}f".format(this).toDouble()
}