package com.example.myapplication.DataHolders

import android.content.Context
import android.hardware.display.DisplayManager
import android.os.Build
import android.util.Log
import android.view.Display

object ScreenDataHolder {

    fun getDisplays(context: Context) {
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
            val manager: DisplayManager =  context.getSystemService(Context.DISPLAY_SERVICE) as DisplayManager
            val displays = manager.displays
            displays.forEach {
                Log.d("Teste", "getDisplays: " + it.name + "+" + it.displayId)
                Log.d("Teste", "getDisplays: " + (it.displayId == Display.DEFAULT_DISPLAY))
            }
            Log.d("Teste", "display amount: " + displays.size)
        }
    }
}