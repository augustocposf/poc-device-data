package com.example.myapplication.DataHolders

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager

object BatteryDataHolder {

    fun getBatteryData(context: Context, onReceived: (String, BroadcastReceiver) -> Unit) {
        val filter  = IntentFilter(Intent.ACTION_BATTERY_CHANGED)
        val receiver: BroadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                val batteryHealth : Int = intent!!.getIntExtra(BatteryManager.EXTRA_HEALTH, 0)
                when(batteryHealth){
                    BatteryManager.BATTERY_HEALTH_COLD -> onReceived("cold", this)
                    BatteryManager.BATTERY_HEALTH_DEAD -> onReceived("dead", this)
                    BatteryManager.BATTERY_HEALTH_GOOD -> onReceived("good", this)
                    BatteryManager.BATTERY_HEALTH_OVERHEAT -> onReceived("overheat", this)
                    BatteryManager.BATTERY_HEALTH_OVER_VOLTAGE -> onReceived("over voltage", this)
                    BatteryManager.BATTERY_HEALTH_UNKNOWN -> onReceived("unknown", this)
                    BatteryManager.BATTERY_HEALTH_UNSPECIFIED_FAILURE -> onReceived("unspecified", this)
                }
            }
        }
        context.registerReceiver(receiver, filter)
    }
}