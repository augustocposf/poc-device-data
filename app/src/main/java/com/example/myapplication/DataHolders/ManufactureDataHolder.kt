package com.example.myapplication.DataHolders

import android.os.Build

object ManufactureDataHolder {
    fun getModel(): String = Build.MODEL

    fun getBoard(): String = Build.BOARD

    fun getDevice(): String = Build.DEVICE

    fun getManufacturer(): String = Build.MANUFACTURER

    fun getBrand(): String = Build.BRAND

    fun getHardware(): String = Build.HARDWARE

    fun getBootloader(): String = Build.BOOTLOADER

}