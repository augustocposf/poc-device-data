package com.example.myapplication.DataHolders

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.telephony.SubscriptionManager
import android.telephony.TelephonyManager
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getSystemService


object SIMDataHolder {
    fun getSIMNumber(context: Context): String {
        
        if(ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            val telemamanger =
                context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager?
            return telemamanger!!.simOperator
        }
        return "Não disponivel"
    }

    fun getSimAmount(context: Context): Int {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
           val subsManager = context.getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE) as SubscriptionManager?
            return subsManager!!.activeSubscriptionInfoCountMax
        }
        return -1
    }

    fun getPhoneCount(context: Context): String {
        if(ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            val telemamanger =
                    context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager?
            if(Build.VERSION.SDK_INT > Build.VERSION_CODES.N && Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
                return telemamanger!!.phoneCount.toString()
            }
        }
        return "Não disponivel"
    }

    fun getModemCount(context: Context): String {
        if(ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            val telemamanger =
                    context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager?
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                return telemamanger!!.activeModemCount.toString()
            }
        }
        return "Não disponivel"
    }
}