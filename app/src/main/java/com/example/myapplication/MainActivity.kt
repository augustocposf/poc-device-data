package com.example.myapplication

import android.Manifest
import android.content.BroadcastReceiver
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.DataHolders.*
import com.example.myapplication.model.Data

class MainActivity : AppCompatActivity() {
    val PHONE_REQUEST_CODE = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val hardwareDataRecyclerView: RecyclerView = findViewById(R.id.hardware_data_recycler_view)
        val hardwareData: MutableList<Data> = mutableListOf()
        val hardwareDataAdapter = HardwareDataAdapter(hardwareData)
        val simAmount = SIMDataHolder.getSimAmount(this)
        hardwareDataRecyclerView.adapter = hardwareDataAdapter



        hardwareDataAdapter.addHardwareData(Data(ManufactureDataHolder.getModel(), "Modelo"))
        hardwareDataAdapter.addHardwareData(
            Data(
                ManufactureDataHolder.getManufacturer(),
                "Fabricante"
            )
        )
        hardwareDataAdapter.addHardwareData(Data(ManufactureDataHolder.getBrand(), "Marca"))


        if (simAmount >= 0) {
            hardwareDataAdapter.addHardwareData(
                Data(
                    simAmount.toString(),
                    "Numero de chips"
                )
            )
        } else {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.READ_PHONE_STATE
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                Log.d("Teste", "onCreate: " + SIMDataHolder.getSIMNumber(this))
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        arrayOf(Manifest.permission.READ_PHONE_STATE),
                        PHONE_REQUEST_CODE
                    )
                }
            }
            if( Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
                hardwareDataAdapter.addHardwareData(
                    Data(
                        SIMDataHolder.getPhoneCount(this),
                        "Numero de chips"
                    )
                )
            }else{
                hardwareDataAdapter.addHardwareData(
                    Data(
                        SIMDataHolder.getModemCount(this),
                        "Numero de chips"
                    )
                )
            }



        }


        hardwareDataAdapter.addHardwareData(
            Data(
                StorageDataHolder.getInternalMemorySize().toString(), "Memória interna (bytes)"
            )
        )
        hardwareDataAdapter.addHardwareData(
            Data(
                StorageDataHolder.getInternalMemorySizeGB(),
                "Memória interna (GB)"
            )
        )
        hardwareDataAdapter.addHardwareData(Data(StorageDataHolder.getRamAmountGB(), "Ram (Arredondada)"))
        hardwareDataAdapter.addHardwareData(
            Data(
                StorageDataHolder.getRamUpdated(this),
                "Ram (Obtida)"
            )
        )

        hardwareDataAdapter.addHardwareData(
            Data(
                StorageDataHolder.hasSdCard(),
                "Possui cartão de memoria"
            )
        )
        hardwareDataAdapter.addHardwareData(
            Data(
                StorageDataHolder.getSdCardSize().toString(),
                "Tamanho cartão de memoria"
            )
        )
        hardwareDataAdapter.addHardwareData(
            Data(
                StorageDataHolder.getSdCardSizeGB(),
                "Tamanho cartão de memoria (GB)"
            )
        )
        hardwareDataAdapter.addHardwareData(Data(OSDataHolder.getOSVersion(), "Versão da API"))
        hardwareDataAdapter.addHardwareData(
            Data(
                OSDataHolder.getOSVersionName(),
                "Versão do sistema (Código)"
            )
        )
        StorageDataHolder.getData()
        val onResult = { result: String, receiver: BroadcastReceiver ->
            hardwareDataAdapter.addHardwareData(
                Data(
                    result,
                    "Status Bateria"
                )
            )
            unregisterReceiver(receiver)
        }
        BatteryDataHolder.getBatteryData(this, onResult)
    }

}